defmodule BDF.Font do
  alias BDF.Parser

  defstruct [:format, :font, :size, :font_bounding_box, :metrics_set,
             :properties, :chars]


  def encode(font, string) when is_binary(string) do
    string |> String.codepoints
           |> Enum.map(&String.to_charlist(&1))
           |> Enum.map(&(encode(font, &1)))
           |> Enum.zip
           |> Enum.map(&Tuple.to_list(&1))
           |> Enum.map(&Enum.reduce(&1, <<>>, fn (x, acc) -> acc <> x end))
  end

  def encode(font, char) do
    [codepoint] = char
    char = font |> Map.get(:chars)
                |> Map.get(codepoint)

    {bbw, bbh, bbxoffx, bbxoffy} = char.bbx
    bitmap = char |> Map.get(:bitmap)
                  |> Enum.chunk_every(round(bbw / 8))
                  |> Enum.map(&Enum.reduce(&1, <<>>, fn (x, acc) -> acc <> x end))

    bitmap
  end

  def prettyprint(char) do
    Enum.map(
      char, fn (l) ->
        Parser.to_bits(l) |> Enum.map(
          fn (bit) ->
            if bit == 1, do: IO.write("#"), else: IO.write(" ")
          end)
          IO.write "\n"
      end)
      :ok
  end
end
