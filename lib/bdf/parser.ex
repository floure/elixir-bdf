defmodule BDF.Parser do
  alias BDF.{Char, Font}


  ###

  def i(string), do: String.to_integer(string)
  def f(string), do: String.to_float(string)

  ###

  def parse_header(_, map \\ %{})
  def parse_header([], map), do: map
  def parse_header([line|tail], map) do
    new = case String.split(line) do
      ["STARTFONT", n] -> %{format: f(n)}
      ["FONT", font] -> %{font: font}
      ["SIZE", point_size, x_res, y_res] ->
        %{size: {i(point_size), i(x_res), i(y_res)}}
      ["FONTBOUNDINGBOX", fbb_x, fbb_y, x_off, y_off] ->
        %{font_bounding_box: {i(fbb_x), i(fbb_y), i(x_off), i(y_off)}}
      ["METRICSSET", n] -> %{metrics_set: i(n)}
      _ ->
        IO.puts "Unknown line: #{line}"
        %{}
    end

    parse_header tail, Map.merge(map, new)
  end

  def parse_properties(lines, map \\ %{})
  def parse_properties([], map), do: map
  def parse_properties([line|tail], map) do
    updated = case String.split(line) do
      ["STARTPROPERTIES", n] -> map
      ["ENDPROPERTIES"] -> map
      [key|value] -> Map.merge map, %{
        key => String.slice(
          line, String.length(key), String.length(line) - String.length(key)
        )
      }
    end
    parse_properties(tail, updated)
  end

  def parse_chars(lines, map \\ %{})
  def parse_chars([], map), do: map
  def parse_chars([line|tail], map) do
    case String.split(line) do
      ["CHARS", n] -> parse_chars(tail, map)
      ["STARTCHAR", c] ->
        {raw_char, next_chars} = Enum.split_while(
          tail, fn (l) -> not String.starts_with?(l, "ENDCHAR") end
        )
        char = Char.parse(raw_char)
        {codepoint, _} = String.slice(c, 2, 4) |> Integer.parse(16)
        parse_chars(next_chars, Map.merge(map, %{codepoint => char}))
      ["ENDFONT"] -> parse_chars(tail, map) # tail == []
      _ -> parse_chars(tail, map)
    end
  end

  defp check_file_bounds(lines) do
    unless lines |> List.first |> String.starts_with?("STARTFONT") do
      raise "Input do not starts with STARTFONT"
    end

    unless lines |> List.last |> String.starts_with?("ENDFONT") do
      raise "Input do not ends with ENDFONT"
    end
  end

  def parse(lines) do
    check_file_bounds(lines)

    {raw_header, header_tail} = Enum.split_while(
      lines, fn (l) -> not String.starts_with?(l, "STARTPROPERTIES") end
    )
    {raw_properties, properties_tail} = Enum.split_while(
      header_tail, fn (l) -> not String.starts_with?(l, "CHARS") end
    )
    {raw_chars, _} = Enum.split_while(
      properties_tail, fn (l) -> not String.starts_with?(l, "ENDFONT") end
    )

    header = parse_header(raw_header)
    properties = %{properties: parse_properties(raw_properties)}
    chars = %{chars: parse_chars(raw_chars)}

    struct(Font) |> struct(header)
                 |> struct(properties)
                 |> struct(chars)
  end

  def to_bits(bin) when is_binary(bin), do: to_bits(bin, [])
  defp to_bits(<<b :: size(1), bits :: bitstring>>, acc) when is_bitstring(bits) do
    to_bits(bits, [b|acc])
  end
  defp to_bits(<<>>, acc), do: acc |> Enum.reverse
end
