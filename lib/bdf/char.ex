defmodule BDF.Char do
  alias BDF.Char
  import BDF.Parser, only: [i: 1]

  defstruct [:encoding, :swidth, :dwidth, :swidth1, :dwith1, :vvector, :bbx,
             :bitmap]

  defp parse_header(lines, map \\ %{})
  defp parse_header([], map), do: map
  defp parse_header([line|tail], map) do
    new = case String.split(line) do
      ["ENCODING", n] -> %{encoding: i(n)}
      ["SWIDTH", swx0, swx1] -> %{swidth: {i(swx0), i(swx1)}}
      ["DWIDTH", dwx0, dwx1] -> %{dwidth: {i(dwx0), i(dwx1)}}
      ["VVECTOR", xoff, yoff] -> %{vvector: {i(xoff), i(yoff)}}
      ["BBX", bbw, bbh, bbxoff0x, bbyoff0y] ->
        %{bbx: {i(bbw), i(bbh), i(bbxoff0x), i(bbyoff0y)}}
      _ -> :noop
    end

    parse_header tail, Map.merge(map, new)
  end

  defp pad_binary(bin, len) do
    unless byte_size(bin) < len do
      bin
    else
      String.duplicate(<<0x0>>, byte_size(bin)) <> bin
    end
  end

  def parse(lines) do
    {head, ["BITMAP"|bitmap_raw]} = Enum.split_while(
      lines,
      fn (l) -> not String.starts_with?(l, "BITMAP") end
    )

    bitmap_values = Enum.map(
      bitmap_raw, fn (str) ->
        {int ,_} = Integer.parse(str, 16)
        <<int::integer>>
      end
    )

    bitmap_max_size = bitmap_values
                      |> Enum.reduce(0, fn(x, acc) -> max(byte_size(x), acc) end)
    bitmap = bitmap_values
             |> Enum.map(&pad_binary(&1, bitmap_max_size))

    struct %Char{bitmap: bitmap}, parse_header(head)
  end
end
