defmodule BDF do
  @moduledoc """
  Documentation for BDF.
  """

  alias BDF.Parser

  def load(path) do
    File.stream!(path, [], :line)
      |> Stream.map(&(String.trim(&1)))
      |> Enum.to_list
      |> parse
  end

  def parse(lines) do
    Parser.parse(lines)
  end
end
