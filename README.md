# Elixir BDF

**This project has been moved on [gitlab.com](https://gitlab.com/Fnux/elixir-bdf).**

A WIP implementation of the [BDF (Glyph Bitmap Distribution
Format)](https://en.wikipedia.org/wiki/Glyph_Bitmap_Distribution_Format) in
elixir.
